import React, { Component } from 'react'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'

export default class CryptoList extends Component {
    render() {
        const {json, fiat, changeFunction, refreshList} = this.props;
        const items = Object.keys(json).map( (key) => {
          return <RenderItem key={key} item={json[key]} fiat={fiat} changeFunction={changeFunction} />
        });
  
        return (
            <table className="table">
              <thead>
                <tr>
                  <th title="rank">Rank</th>
                  <th title="symbol">Symbol</th>
                  <th title="price">Price ({fiat})</th>
                  <th title="exchange">24h Exchange</th>
                  <th title="refresh"><button className="button is-info" onClick={() => refreshList()}>
                      <FontAwesomeIcon icon="sync-alt" /><span style={{marginLeft: '10px'}}>Refresh</span>
                    </button>
                  </th>
                </tr>
              </thead>
              <tbody>
                {items}
              </tbody>
            </table>
        )
    }
  }
  
  class RenderItem extends Component {
    render() {
      const {item, fiat, changeFunction} = this.props;
  
      let currFiat = fiat
      if (!item.quotes[currFiat]) {
        currFiat = 'USD'
      }
  
      return (
        <tr>
          <th>{item.rank}</th>
          <td>{item.symbol}</td>
          <td>{item.quotes[currFiat].price}</td>
          <td>{item.quotes[currFiat].percent_change_24h}</td>
          <td><i onClick={() => changeFunction(item)} style={{cursor: 'pointer'}}><FontAwesomeIcon icon="angle-right" color="grey" /></i></td>
        </tr>
      )
    }
  }