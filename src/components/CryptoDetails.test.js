import React from 'react';
import { shallow, mount } from 'enzyme';
import renderer from 'react-test-renderer';
import CryptoDetails from './CryptoDetails';

describe('<CryptoDetails />', () => {
  let wrapper;
  beforeEach(() => {
    const item = {
      circulating_supply: 17385675,
      id: 1,
      last_updated: 1542732073,
      max_supply: 21000000,
      name: "Bitcoin",
      quotes: {
        'USD': {
          market_cap: 82730199193,
          percent_change_1h: 0.62,
          percent_change_7d: -25.26,
          percent_change_24h: -7.4,
          price: 4758.5267292,
          volume_24h: 8678616004.77215,
        }
      },
      rank: 1,
      symbol: "BTC",
      total_supply: 17385675,
      website_slug: "bitcoin",
    }
    wrapper = shallow(<CryptoDetails item={item} fiat='USD' BTCinUSD='4758.5267292' />);
  });

  it('displays main crypto attributes', () => {
    expect(wrapper.find('.box .rank').html()).toMatch('<span class="rank">Rank: <b>1</b></span>');
    expect(wrapper.find('.box .symbol').html()).toMatch('<span class="symbol">Symbol: <b>BTC</b></span>');
    expect(wrapper.find('.box .price').html()).toMatch('<span class="price">Price: <b>4758.5267292 (USD)</b></span>');
  });
});