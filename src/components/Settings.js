import React, { Component } from 'react'

export default class Settings extends Component {

    state = {
        options: [
            {
                name: 'USD',
                value: 'USD',
            },
            {
                name: 'EUR',
                value: 'EUR',
            },
            {
                name: 'CNY',
                value: 'CNY',
            },
        ],
        value: '?',
    };

    handleChange = (event) => {
        this.setState({ value: event.target.value });
    };

    render() {
        const {active, saveSettings} = this.props;
        return (
            <div className={"modal " + (active ? 'is-active' : '')}>
                <div className="modal-background"></div>
                <div className="modal-card">
                    <header className="modal-card-head">
                        <p className="modal-card-title">Settings</p>
                    </header>
                    <section className="modal-card-body">
                    <div className="select">
                    <select onChange={this.handleChange} value={this.state.value}>
                        {this.state.options.map(item => (
                            <option key={item.value} value={item.value}>
                                {item.name}
                            </option>
                        ))}
                    </select>
                    </div>
                    </section>
                    <footer className="modal-card-foot" style={{justifyContent: 'flex-end'}}>
                        <button className="button is-success" onClick={() => saveSettings(this.state.value)}>Save changes</button>
                    </footer>
                </div>
            </div>
        )
    }
}