import React, { Component } from 'react'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'

export default class Header extends Component {
    render() {
        const {openSettings} = this.props;
        return (
            <nav className="navbar is-primary">
                <div className="navbar-brand">
                    <span className="navbar-item title is-size-4">
                        Crypto React
                    </span>
                </div>
                <div className="navbar-menu level" style={{justifyContent: 'flex-end'}}>
                    <div className="navbar-start is-marginless">
                        <button className="button is-primary" onClick={() => openSettings()} style={{height: '100%'}}>
                            <FontAwesomeIcon icon="wrench" />
                        </button>
                    </div>
                </div>
            </nav>
        )
      }
}