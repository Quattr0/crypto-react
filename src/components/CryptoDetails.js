import React, { Component } from 'react'

export default class CryptoDetails extends Component {
    render() {
        const {item, fiat, BTCinUSD} = this.props;

        let currFiat = fiat
        if (item && !item.quotes[currFiat]) {
            currFiat = 'USD'
        }

        return (
            <div className="box" style={{margin: '100px 0 0 50px', minWidth: '500px', position: 'sticky', top: '80px'}}>
                {item ? 
                (
                    <div className="columns is-multiline">
                        <div className="column is-full">
                            <span className="title is-size-5">{item.name}</span>
                        </div>
                        <div className="column is-half">
                            <span className="rank">Rank: <b>{item.rank}</b></span><br />
                            <span className="symbol">Symbol: <b>{item.symbol}</b></span><br />
                        </div>
                        <div className="column is-half">
                            <span className="price">Price: <b>{item.quotes[currFiat].price} ({currFiat})</b></span><br />
                            <span className="24h-volume">24h volume: <b>{item.quotes[currFiat].volume_24h} ({currFiat})</b></span><br />
                            <span className="market-cap">Market cap: <b>{item.quotes[currFiat].market_cap} ({currFiat})</b></span><br />
                        </div>
                        <div className="column is-half">
                            <span className="1h-change">1h change ({currFiat}): <b>{item.quotes[currFiat].percent_change_1h}</b></span><br />
                            <span className="24h-change">24h change ({currFiat}): <b>{item.quotes[currFiat].percent_change_24h}</b></span><br />
                            <span className="7d-change">7d change ({currFiat}): <b>{item.quotes[currFiat].percent_change_7d}</b></span><br />
                        </div>
                        <div className="column is-half">
                            <span className="price-in-BTC">Price in BTC: <b>{item.quotes['USD'].price / BTCinUSD}</b></span><br />
                            <span className="total-supply">Total supply: <b>{item.total_supply}</b></span><br />
                            <span className="available-supply">Available supply: <b>{item.circulating_supply}</b></span><br />
                        </div>
                    </div>
                ) 
                : 
                (
                    <span className="subtitle">Click the arrow to the right of the cryptocurrency for more details</span>)
                }
                
            </div>
        )
    }
}