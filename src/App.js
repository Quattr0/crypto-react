import React, { Component } from 'react'
import { observable, action } from 'mobx';
import {inject, observer} from 'mobx-react'
import './App.css';
import 'bulma/css/bulma.css'

import { library } from '@fortawesome/fontawesome-svg-core'
import { faWrench, faAngleRight, faSyncAlt } from '@fortawesome/free-solid-svg-icons'

import Header from './components/Header'
import CryptoList from './components/CryptoList'
import CryptoDetails from './components/CryptoDetails'
import Settings from './components/Settings'

library.add(faWrench, faAngleRight, faSyncAlt)

@inject('Store')
@observer
class App extends Component {

  @observable selectedCrypto
  @observable settingsOpened = false

  @action selectCrypto(item) {
    this.selectedCrypto = item
  }

  @action openSettings() {
    this.settingsOpened = true
  }

  @action saveSettings(value) {
    const {Store} = this.props
    Store.fiat = value
    this.settingsOpened = false
    this.refreshList()
  }

  @action refreshList() {
    console.log('Refreshing list')
    const {Store} = this.props
    Store.fetchCrypto()
  }

  render() {
    const {Store} = this.props
    return (
      <div className="App">
        <Header openSettings={this.openSettings.bind(this)} />
        <div className="container level" style={{justifyContent: 'space-around', alignItems: 'flex-start'}}>
          <CryptoList json={Store.cryptoArray} fiat={Store.fiat} changeFunction={this.selectCrypto.bind(this)} refreshList={this.refreshList.bind(this)} />
          <CryptoDetails item={this.selectedCrypto} fiat={Store.fiat} BTCinUSD={Store.BTCinUSD} />
        </div>
        <Settings active={this.settingsOpened} saveSettings={this.saveSettings.bind(this)} />
      </div>
    )
  }
}

export default App;
