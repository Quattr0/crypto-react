import {observable, action, runInAction} from 'mobx'
import * as api from '../api/Api'

class Store {
    @observable cryptoArray = []
    @observable fiat = 'USD'
    @observable BTCinUSD = 0

    constructor() {
        this.fetchCrypto()
    }

    @action fetchCrypto = async () => {
        try {
            const fetchedCryptos = await api.get(this.fiat)

            runInAction(() => {
                this.cryptoArray = fetchedCryptos.data
                this.BTCinUSD = fetchedCryptos.data['1'].quotes[this.fiat].price
            });
        } catch (error) {
            runInAction(() => {
                console.log('Error fetching crypto: ', error)
            })
        }
    }
}

const store = new Store()
export default store