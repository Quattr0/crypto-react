const baseUrl = 'https://api.coinmarketcap.com/v2'

export function get(fiat) {
  return fetch(`${baseUrl}/ticker/?sort=rank&convert=` + fiat)
          .then( response => response.json() )
          .catch( err => err )
}