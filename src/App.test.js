import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';
import {Provider} from 'mobx-react'
import Store from './store/Store'

it('renders without crashing', () => {
  const div = document.createElement('div');
  ReactDOM.render(<Provider Store={Store}><App /></Provider>, div);
  ReactDOM.unmountComponentAtNode(div);
});
